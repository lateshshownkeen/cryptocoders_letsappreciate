package com.example.prajnashetty.letsappreciate;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class UserProfile extends AppCompatActivity {

    Button btnAppreciation,btnEvents;
    ImageView ivProfile;
    TextView tvDetails;
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    Context context=UserProfile.this;
    String json_url="http://c55944ac.ngrok.io/letsappreciate/getinfo.php";
    ArrayList<Contact2> arrayList= new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        btnAppreciation=(Button)findViewById(R.id.btnAppreciation);
        btnEvents=(Button)findViewById(R.id.btnEvents);
        ivProfile=(ImageView)findViewById(R.id.ivProfile);
        tvDetails=(TextView)findViewById(R.id.tvDetails);
        recyclerView=(RecyclerView)findViewById(R.id.recyclerView);
        layoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);



       btnAppreciation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JsonArrayRequest jsonArrayRequest= new JsonArrayRequest(Request.Method.POST, json_url, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        int count=0;

                        while(count<response.length()){
                            try {
                                JSONObject jsonObject=response.getJSONObject(count);
                                Contact2 contact=new Contact2(jsonObject.getString("Empname"),jsonObject.getString("Description"));
                                arrayList.add(contact);

                                count++;
                            } catch (JSONException e) {

                                e.printStackTrace();
                            }

                        }
                        adapter=new RecyclerAdapter(arrayList);
                        recyclerView.setAdapter(adapter);

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "Error.....", Toast.LENGTH_SHORT).show();
                        error.printStackTrace();
                    }
                });
                MySingleton.getInstance(context).addToRequestque(jsonArrayRequest);





                 }
        });

       btnEvents.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               /*JsonArrayRequest jsonArrayRequest= new JsonArrayRequest(Request.Method.POST, json_url, null, new Response.Listener<JSONArray>() {
                   @Override
                   public void onResponse(JSONArray response) {
                       int count=0;

                       while(count<response.length()){
                           try {
                               JSONObject jsonObject=response.getJSONObject(count);
                              // Contact contact=new Contact(jsonObject.getString("Name"),jsonObject.getString("Email"));
                             //  arrayList.add(contact);

                               count++;
                           } catch (JSONException e) {

                               e.printStackTrace();
                           }

                       }
                       adapter=new RecyclerAdapter(arrayList);
                       recyclerView.setAdapter(adapter);

                   }
               }, new Response.ErrorListener() {
                   @Override
                   public void onErrorResponse(VolleyError error) {
                       Toast.makeText(context, "Error.....", Toast.LENGTH_SHORT).show();
                       error.printStackTrace();
                   }
               });
               MySingleton.getInstance(context).addToRequestque(jsonArrayRequest);*/

           }

       });

    }
}
