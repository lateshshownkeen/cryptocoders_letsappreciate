package com.example.prajnashetty.letsappreciate;

import android.content.Context;
import android.content.Intent;
import android.media.audiofx.AudioEffect;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.content.Intent;
import java.util.ArrayList;

public class RecyclerAdapter2 extends RecyclerView.Adapter<RecyclerAdapter2.MyViewHolder> {


    @NonNull
    ArrayList<Contact> arrayList=new ArrayList<>();

    public  RecyclerAdapter2(ArrayList<Contact> arrayList){

        this.arrayList=arrayList;
    }
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item2,parent,false);
        MyViewHolder myViewHolder=new MyViewHolder(view);
        return myViewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerAdapter2.MyViewHolder holder, int position) {
       holder.Empname.setText(arrayList.get(position).getEmpname());
       holder.Description.setText(arrayList.get(position).getDescription());
        holder.letsAppreciate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                Intent intent = new Intent(context , AppreciationActivity.class);
                context.startActivity(intent);
            }
        });
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
    @Override
    public int getItemCount() {

        return arrayList.size();
    }
    public  static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView Empname,Description;
        Button letsAppreciate;
        public MyViewHolder(View itemView) {
            super(itemView);
            Empname=(TextView)itemView.findViewById(R.id.tvAuthor2);
            Description=(TextView)itemView.findViewById(R.id.AuthorContent2);
            letsAppreciate=(Button)itemView.findViewById(R.id.letsAppreciate);


        }}
}
