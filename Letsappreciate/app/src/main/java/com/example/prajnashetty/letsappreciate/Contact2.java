package com.example.prajnashetty.letsappreciate;

public class Contact2 {
    private  String Empname,Description;
    public Contact2(String Empname,String Description){
        this.setEmpname(Empname);
        this.setDescription(Description);
    }

    public String getEmpname() {
        return Empname;
    }

    public void setEmpname(String Empname) {
        this.Empname = Empname;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }
}
