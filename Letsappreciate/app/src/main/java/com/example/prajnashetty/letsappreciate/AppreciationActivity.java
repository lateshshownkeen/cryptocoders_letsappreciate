package com.example.prajnashetty.letsappreciate;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class AppreciationActivity extends AppCompatActivity {

    Button button1, button2,button3;
    ImageView imageView2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appreciation);


        button1=(Button) findViewById(R.id.button1);
        button2=(Button) findViewById(R.id.button2);
        imageView2 = (ImageView)findViewById(R.id.imageView2);
        button3 =(Button) findViewById(R.id.button3);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("sms: " + "8451902663"));
                i.putExtra("sms_body","Hey there");
                startActivity(i);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Intent.ACTION_SENDTO);
                i.setData(Uri.parse("mailto: " + "aanchalrohira97@gmail.com"));
                i.putExtra(Intent.EXTRA_SUBJECT,"Appreciation Post");
                i.putExtra(Intent.EXTRA_TEXT,"Wanted to appreciate you for your performance");
                startActivity(i);
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("smsto:" + "8451902663");
                Intent i = new Intent(Intent.ACTION_SENDTO, uri);
                i.putExtra("sms_body", "hey there");
                i.setPackage("com.whatsapp");
                startActivity(i);
            }
        });
}}
