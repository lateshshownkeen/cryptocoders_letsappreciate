package com.example.prajnashetty.letsappreciate;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    Context context=MainActivity.this;
    String json_url="http://66784df1.ngrok.io/letsappreciate/getbday.php";

    ArrayList<Birthday> arrayList= new ArrayList<>();
    Button btn1;
Button btnContribute,btnAppreciate,btnUpcomingEvents,btnUsers,btnProfile,btnEventDone,btnGoals;
            TextView tvUser;
            ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
btnContribute=(Button)findViewById(R.id.btnContribute);
btnAppreciate=(Button)findViewById(R.id.btnAppreciate);
btnEventDone=(Button)findViewById(R.id.btnEventDone);
btnUpcomingEvents=(Button)findViewById(R.id.btnUpcomingEvents);
btnUsers=(Button)findViewById(R.id.btnUpcomingEvents);
btnProfile=(Button)findViewById(R.id.btnProfile);
         btnGoals = (Button)findViewById(R.id.btnGoals);
         imageView = (ImageView)findViewById(R.id.imageView);
tvUser=(TextView)findViewById(R.id.tvUser);
        final Dialog myDialog = new Dialog(this);
        //btn1=(Button)findViewById(R.id.btn1);
        JsonArrayRequest jsonArrayRequest= new JsonArrayRequest(Request.Method.POST, json_url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                int count=0;

                while(count<response.length()){
                    try {
                        JSONObject jsonObject=response.getJSONObject(count);
                        Birthday contact = new Birthday(jsonObject.getString("bday"));
                        arrayList.add(contact);

                        count++;
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                }
                System.out.print(arrayList);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Error.....", Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        });
        MySingleton.getInstance(context).addToRequestque(jsonArrayRequest);


        Calendar calendar=Calendar.getInstance();
        int date=calendar.get(Calendar.DAY_OF_MONTH);
        int month=calendar.get(Calendar.MONTH);
        month++;
        int tdate=22;
        int tmonth=9;
        if(true){
            //   btn1.setOnClickListener(new View.OnClickListener() {
            //     @Override
            //     public void onClick(View v) {
            TextView txtclose;
            myDialog.setContentView(R.layout.custompopup);
            txtclose=(TextView)myDialog.findViewById(R.id.txtclose);
            txtclose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myDialog.dismiss();
                }
            });
            myDialog.show();
            //      }
            //   })




btnUsers.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        startActivity(new Intent(MainActivity.this,Collegues.class));
    }
});

btnProfile.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        startActivity(new Intent(MainActivity.this,UserProfile.class));

    }
});
btnContribute.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        startActivity(new Intent(MainActivity.this,ContributeActivity.class));
    }


});

btnGoals.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        startActivity(new Intent(MainActivity.this,Goals.class));

    }
});

btnEventDone.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        startActivity(new Intent(MainActivity.this,EventDoneActivity.class));
    }
});

btnAppreciate.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        startActivity(new Intent(MainActivity.this,AppreciationActivity.class));
    }
});
btnUpcomingEvents.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        startActivity(new Intent(MainActivity.this,UpcomingEvents.class));
    }
});
    }
}}
