package com.example.prajnashetty.letsappreciate;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SabkeContributions extends AppCompatActivity {
    RecyclerView recyclerView2;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;
    Context context=SabkeContributions.this;
    String json_url="http://c55944ac.ngrok.io/letsappreciate/getcontriinfo.php";
    ArrayList<Contact> arrayList= new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sabke_contributions);

        recyclerView2=(RecyclerView)findViewById(R.id.recyclerView2);
        layoutManager=new LinearLayoutManager(this);
        recyclerView2.setLayoutManager(layoutManager);
        recyclerView2.setHasFixedSize(true);
        JsonArrayRequest jsonArrayRequest= new JsonArrayRequest(Request.Method.POST, json_url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                int count=0;

                while(count<response.length()){
                    try {
                        JSONObject jsonObject=response.getJSONObject(count);
                        Contact contact=new Contact(jsonObject.getString("Empname"),jsonObject.getString("Description"));
                        arrayList.add(contact);

                        count++;
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                }
                adapter=new RecyclerAdapter2(arrayList);
                recyclerView2.setAdapter(adapter);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Error.....", Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        });
        MySingleton.getInstance(context).addToRequestque(jsonArrayRequest);

    }
}
