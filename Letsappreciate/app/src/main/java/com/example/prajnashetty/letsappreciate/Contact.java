package com.example.prajnashetty.letsappreciate;

public class Contact {
    private  String Empname,Description;
    public Contact(String Empid,String Description){
        this.setEmpname(Empid);
        this.setDescription(Description);
    }


    public String getEmpname() {
        return Empname;
    }

    public void setEmpname(String empname) {
        Empname = empname;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;

    }
}
