package com.example.prajnashetty.letsappreciate;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class UpcomingEvents extends AppCompatActivity {
    Button evbt1, evbt2, evbt3;
    ImageView img1, img2, img3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upcoming_events);

        evbt1 = (Button) findViewById(R.id.evbt1);
        evbt2 = (Button) findViewById(R.id.evbt2);
        evbt3 = (Button) findViewById(R.id.evbt3);
        img1 = (ImageView) findViewById(R.id.img1);
        img2 = (ImageView) findViewById(R.id.img2);
        img3 = (ImageView) findViewById(R.id.img3);

        evbt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(UpcomingEvents.this, "Your request has been sent", Toast.LENGTH_SHORT).show();
            }
        });


        evbt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(UpcomingEvents.this, "Your request has been sent", Toast.LENGTH_SHORT).show();
            }
        });


        evbt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(UpcomingEvents.this, "Your request has been sent", Toast.LENGTH_SHORT).show();
            }
        });
    }
    }

