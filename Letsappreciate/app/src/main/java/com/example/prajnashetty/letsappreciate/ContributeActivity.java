package com.example.prajnashetty.letsappreciate;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class ContributeActivity extends AppCompatActivity {
Button btnSubmitContribution,btnViewContribution;
EditText etContribute;
    String server_url="http://c55944ac.ngrok.io/letsappreciate/Contribute.php";
    AlertDialog.Builder builder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contribute);

        btnSubmitContribution=(Button)findViewById(R.id.btnSubmitContribution);
        btnViewContribution=(Button)findViewById(R.id.btnViewContribution);
        etContribute=(EditText)findViewById(R.id.etContribute);
        builder=new AlertDialog.Builder((ContributeActivity.this));
        btnSubmitContribution.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String description,empid;
                description=etContribute.getText().toString();
                //System.out.println(description);
                empid="0000000001";
               // System.out.println(email);


                StringRequest stringRequest= new StringRequest(Request.Method.POST, server_url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        builder.setTitle("Server Response");
                        builder.setMessage("Response:" +response);
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                etContribute.setText("");

                            }
                        });
                        AlertDialog alertDialog=builder.create();
                        alertDialog.show();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ContributeActivity.this," "+error,Toast.LENGTH_SHORT).show();
                        error.printStackTrace();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> params=new HashMap<String, String>();
                        params.put("description",description);
                        params.put("empid",empid);

                        return params;
                    }
                };
                MySingleton.getInstance(ContributeActivity.this).addToRequestque(stringRequest);




//*****************************************************************************************************************************
            }
        });

        btnViewContribution.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ContributeActivity.this,SabkeContributions.class));
            }
        });

    }
}
