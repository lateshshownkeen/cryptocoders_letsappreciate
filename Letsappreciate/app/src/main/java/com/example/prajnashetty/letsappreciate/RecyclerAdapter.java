package com.example.prajnashetty.letsappreciate;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder>{
    Context context;
    @NonNull
   ArrayList<Contact2> arrayList=new ArrayList<>();

    public  RecyclerAdapter(ArrayList<Contact2> arrayList){

        this.arrayList=arrayList;
    }
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item,parent,false);
        MyViewHolder myViewHolder=new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.Empname.setText(arrayList.get(position).getEmpname());
       holder.Description.setText(arrayList.get(position).getDescription());

    }

    @Override
    public int getItemCount() {

        return arrayList.size();
    }
    public  static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView Empname,Description;
        public MyViewHolder(View itemView) {
            super(itemView);
           Empname=(TextView)itemView.findViewById(R.id.tvAuthor);
            Description=(TextView)itemView.findViewById(R.id.AuthorContent);
        }}}
