package com.example.prajnashetty.letsappreciate;

public class Birthday {

    private String bday;
    public Birthday(String bday){
        this.setBday(bday);
    }

    public String getBday() {
        return bday;
    }

    public void setBday(String bday) {
        this.bday = bday;
    }
}
