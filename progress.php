<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Be Appreciated</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
</head>
<body>
  <nav class="light-blue lighten-1" role="navigation">
    <div class="nav-wrapper container col s6"><a id="logo-container" href="index.html" class="brand-logo">Be Appreciated</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="#">Your Profile</a></li>
      </ul>

      <ul id="nav-mobile" class="sidenav">
        <li><a href="#">Your Profile</a></li>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
  </nav>
  
    <div class="container">
      <br><br>
      <h3 class="header center blue-text">Check Progress</h3>
  </div>


  <div class="container">
    <div class="section">
        <div class="row">
          <div class="col s12 m12">
      <!--   Icon Section   -->
        <!--<ul class="collapsible">
          <li>
            <div class="collapsible-header active"><i class="material-icons">filter_drama</i>Emp 1</div>
            <div class="collapsible-body"><div class="activator" id="chart_div" style="height:200px; width: 500px;display : inline-block"></div>
            <div style="display:inline-block;position: absolute;">User Information</div>
            <script>
    google.charts.load('current', {packages: ['corechart', 'line']});
google.charts.setOnLoadCallback(drawLogScales);

function drawLogScales() {
      var data = new google.visualization.DataTable();
      data.addColumn('number', 'Year');
      data.addColumn('number', '');
     // data.addRows(rows);
      data.addRows([
        [0, 0],    [1, 10],   [2, 20],  [3, 30],   [4, 40],  [5, 30],
        [6, 50],   [7, 70],  [8, 80],  [9, 90],  [10, 80]
      ]);

      var options = {
        hAxis: {
          title: 'Year',
          logScale: false
        },
        vAxis: {
          title: 'Appreciations',
          logScale: false
        },
        colors: ['#00f']
      };

      var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
      chart.draw(data, options);
    }
  </script>
          </li>







          <li>
            <div class="collapsible-header"><i class="material-icons">filter_drama</i>First</div>
            <div class="collapsible-body"><img class="activator" src="images/data.jpg" style="height: 20%; width: 26%;"></div>
          </li>
          <li>
              <div class="collapsible-header"><i class="material-icons">filter_drama</i>First</div>
            <div class="collapsible-body"><img class="activator" src="images/data.jpg" style="height: 20%; width: 26%;"></div>
          </li>
          <li>
              <div class="collapsible-header"><i class="material-icons">filter_drama</i>First</div>
            <div class="collapsible-body"><img class="activator" src="images/data.jpg" style="height: 20%; width: 26%;"></div>
          </li>
        </ul>
             -->
             <?php 
             $user="root";
$password="";
$host="localhost";
$db_name="letsappreciate";
$conn=mysqli_connect($host,$user,$password,$db_name);
$sql1="SELECT Count(*) as abc from empdetails where rating=1";
$result1 = mysqli_query($conn, $sql1);
$row1=mysqli_fetch_assoc($result1);

$sql2="SELECT Count(*) as abc from empdetails where rating=2";
$result2 = mysqli_query($conn, $sql2);
$row2=mysqli_fetch_assoc($result2);

$sql3="SELECT Count(*) as abc from empdetails where rating=3";
$result3 = mysqli_query($conn, $sql3);
$row3=mysqli_fetch_assoc($result3);

$sql4="SELECT Count(*) as abc from empdetails where rating=4";
$result4 = mysqli_query($conn, $sql4);
$row4=mysqli_fetch_assoc($result4);

$sql5="SELECT Count(*) as abc from empdetails where rating=5";
$result5 = mysqli_query($conn, $sql5);
$row5=mysqli_fetch_assoc($result5);

$sql6="SELECT Count(*) as abc from empdetails where rating=6";
$result6 = mysqli_query($conn, $sql6);
$row6=mysqli_fetch_assoc($result6);

$sql7="SELECT Count(*) as abc from empdetails where rating=7";
$result7 = mysqli_query($conn, $sql7);
$row7=mysqli_fetch_assoc($result7);

$sql8="SELECT Count(*) as abc from empdetails where rating=8";
$result8 = mysqli_query($conn, $sql8);
$row8=mysqli_fetch_assoc($result8);

$sql9="SELECT Count(*) as abc from empdetails where rating=9";
$result9 = mysqli_query($conn, $sql9);
$row9=mysqli_fetch_assoc($result9);

             echo "<div id='chart_div' style='height:600px;width: 1000px'></div>
             <script>
              google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawBasic);
             function drawBasic() {

      var data = new google.visualization.DataTable();
      data.addColumn('number', 'Ratings');
      data.addColumn('number', '');

      data.addRows([
        [{v: 1, f: 'Emp:'}, ".$row1["abc"]."],
        [{v: 2, f: 'Emp:'}, ".$row2["abc"]."],
        [{v: 3, f: 'Emp:'}, ".$row3["abc"]."],
        [{v: 4, f: 'Emp:'}, ".$row4["abc"]."],
        [{v: 5, f: 'Emp:'}, ".$row5["abc"]."],
        [{v: 6, f: 'Emp:'}, ".$row6["abc"]."],
        [{v: 7, f: 'Emp:'}, ".$row7["abc"]."],
        [{v: 8, f: 'Emp:'}, ".$row8["abc"]."],
        [{v: 9, f: 'Emp:'}, ".$row9["abc"]."],
      ]);

      var options = {
        title: '',
        hAxis: {
          title: 'Rating',
          
          viewWindow: {
            min: 1,
            max: 9
          }
        },
        vAxis: {
          title: 'Number of Employees'
        }
      };

      var chart = new google.visualization.ColumnChart(
        document.getElementById('chart_div'));

      chart.draw(data, options);
      google.visualization.events.addListener(chart, 'select', selectHandler);

function selectHandler(e) {
  var selectedItem = chart.getSelection()[0];
    if (selectedItem) {
      var value = data.getValue(selectedItem.row, selectedItem.column);
       callscatter(selectedItem.row);
    }
  $('html,body').animate(
 { 
  scrollTop: $('#scatter_div').offset().top
 },
 'slow');}}</script>";
  ?>
  <script type="">
    function callscatter(num){
      var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("#abcd").innerHTML =this.responseText;
                
                //document.getElementById("#scatter_div").style.display="block";
                
            }
        };
        xmlhttp.open("POST", "getscatter.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send("row="+num);
        //alert(num);
    }
  </script>
  <script id="abcd">
    google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Years of Experience', 'Number of Appreciations'],
          [12, 2938],
[36, 1474],
[27, 2868],
[8, 6339],
[17, 5500],
[14, 6642],
[34, 2505],
[25, 709],
[12, 6570],
[19, 4852],
[19, 4803],
[7, 2014],
[30, 1475],
[11, 6984],
[23, 3433],
[12, 5461],
[40, 3230],
[20, 1433],
[33, 5346],
[14, 691],
[38, 2285],
[10, 1674],
[12, 4201],
[4, 6887],
[28, 4898],
[15, 1777],
[32, 5212],
[31, 6673],
[27, 5360],
[12, 2123],
[38, 1661],
[10, 956],
[4, 6627],
[37, 6840],
[6, 4251],
[10, 2992],
[11, 6897],
[17, 4948],
[39, 2714],
[23, 3664],
[5, 4116],
[16, 4353],
[39, 1535],
[9, 6024],
[18, 1662],
[2, 2906],
[7, 5980],
[18, 5189],
[19, 1400],
[6, 4291],
[16, 3348],
[34, 2054],
[36, 3169],
[40, 5874],
[39, 4633],
[29, 4996],
[35, 6580],
[24, 1250],
[36, 4046],
[23, 3441],
[22, 4552],
[2, 6531],
[22, 3344],
[8, 4109],
[1, 6967],
[16, 4962],
[32, 2958],
[16, 1978],
[37, 4647],
[33, 798],
[31, 4010],
[28, 6375],
[9, 3739],
[29, 5335],
[18, 3262],
[22, 4363],
[5, 5003],
[7, 6828],
[16, 4139],
[24, 3513],
[25, 2599],
[9, 4776],
[37, 4578],
[25, 1713],
[34, 1246],
[18, 6141],
[20, 4575],
[30, 6376],
[13, 4344],
[35, 4384],
[5, 5155],
[33, 2920],
[2, 2672],
[29, 2924],
[32, 875],
[38, 1552],
[1, 5264],
[28, 4768],
[24, 607],
[16, 4375],
[16, 1814],
[7, 780],
[1, 6812],
[24, 1112],
[10, 2864],
[5, 1218],
[10, 932],
[14, 1833],
[8, 2010],
[32, 2245],
[35, 6334],
[29, 1057],
[28, 4362],
[29, 858],
[16, 5597],
[35, 3876],
[9, 4386],
[2, 6620],
[27, 5160],
[6, 1546],
[20, 5936],
[31, 3910],
[5, 3687],
[39, 613],
[37, 2496],
[29, 1440],
[29, 583],
[34, 1956],
[14, 6889],
[9, 4449],
[22, 6066],
[12, 4660],
[10, 980],
[25, 1942],
[21, 1403],
[27, 4969],
[7, 2943],
[13, 3500],
[33, 5568],
[9, 3558],
[36, 6358],
[39, 3802],
[17, 3708],
[1, 1567],
[27, 4218],
[19, 6164],
[17, 2315],
[40, 1351],
[23, 5776],
[7, 6789],
[12, 3422],
[20, 1014],
[23, 1903],
[3, 542],
[10, 1040],
[16, 1208],
[17, 3297],
[10, 6015],
[4, 1767],
[14, 6158],
[39, 1209],
[39, 5013],
[3, 3603],
[26, 4793],
[21, 1870],
[16, 2370],
[4, 6590],
[13, 6771],
[16, 5556],
[6, 5960],
[22, 1134],
[24, 5436],
[16, 5208],
[2, 5515],
[25, 4691],
[12, 6693],
[22, 4902],
[9, 6402],
[24, 6636],
[40, 1144],
[6, 6826],
[33, 1465],
[32, 5553],
[26, 6135],
[22, 3904],
[17, 2306],
[14, 6137],
[28, 3898],
[5, 3503],
[12, 4524],
[5, 604],
[24, 6946],
[4, 1439],
[6, 3177],
[29, 4937],
[36, 1138],
[24, 1443],
[13, 4098],
[20, 4993],
[23, 2494],
[5, 560],
[33, 1509],
[8, 1007],
[1, 1382],
[36, 5110],
[19, 859],
[11, 991],
[6, 6443],
[21, 987],
[33, 1704],
[5, 750],
[35, 1282],
[13, 6680],
[24, 1719],
[29, 6204],
[23, 3261],
[33, 5350],
[40, 1171],
[8, 2543],
[35, 2402],
[40, 4277],
[12, 3519],
[15, 5129],
[10, 6892],
[29, 5879],
[30, 1817],
[5, 506],
[9, 5578],
[26, 649],
[15, 4998],
[39, 4592],
[31, 2746],
[14, 6128],
[20, 5898],
[1, 5282],
[6, 6806],
[7, 2638],
[17, 6939],
[39, 1350],
[8, 2312],
[24, 5171],
[10, 1825],
[9, 579],
[18, 4980],
[36, 694],
[28, 803],
[21, 6674],
[11, 6914],
[24, 4084],
[24, 1771],
[32, 6962],
[5, 4362],
[4, 2566],
[8, 6940],
[29, 6881],
[11, 4612],
[34, 4741],
[24, 3377],
[4, 5265],
[17, 2265],
[22, 1124],
[37, 6612],
[27, 2368],
[23, 3699],
[35, 727],
[33, 5694],
[33, 5767],
[38, 5778],
[28, 893],
[4, 4226],
[37, 5895],
[24, 1129],
[16, 3702],
[20, 1897],
[19, 2272],
[27, 4774],
[35, 3746],
[22, 3142],
[22, 3050],
[15, 6093],
[20, 2563],
[10, 1917],
[22, 5138],
[35, 5599],
[25, 1708],
[2, 3777],
[4, 2906],
[3, 4807],
[4, 4818],
[10, 6511],
[17, 5792],
[28, 2533],
[33, 1288],
[17, 5518],
[10, 1636],
[14, 6277],
[5, 2689],
[23, 5646],
[30, 2035],
[9, 3898]

          
        ]);
        var options = {
          title: 'Experience vs. Appreciations',
          hAxis: {title: 'Experience', minValue: 0, maxValue: 50},
          vAxis: {title: 'Appreciations', minValue: 0, maxValue: 7000},
          legend: 'none',
         series: {
            0: { color: '#e2431e' },
            1: { color: '#e7711b' },
            2: { color: '#f1ca3a' },
            3: { color: '#6f9654' },
            4: { color: '#1c91c0' },
            5: { color: '#43459d' },
          }
        };
        var chart = new google.visualization.ScatterChart(document.getElementById('scatter_div'));
        chart.draw(data, options);
      }
  </script>
  <div id="scatter_div" style="width: 900px; height: 500px;"></div>
        </div>
      </div>
      </div>

    </div>
    <br><br>
  </div>

  <footer class="page-footer light-blue">
    <div class="container">
      <div class="row">
        <div class="col l12 s12">
          <h5 class="white-text">Company Bio</h5>
          <p class="grey-text text-lighten-4">We are a team of college students working on this project like it's our full time job. Any amount would help support and continue development on this project and is greatly appreciated.</p>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      Made by <a class="orange-text text-lighten-3" href="http://materializecss.com">Materialize</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>
  <script type="text/javascript">
   $(document).ready(function(){
    $('.collapsible').collapsible();
  });
        
  </script>
  </body>
</html>
